﻿using System;

namespace ExceptionsReel
{
    /// <summary>
    /// this interface defines requirements for classes that will 
    /// provide exceptions recording and playback
    /// </summary>
    public interface IExceptionRecorder
    {
        SimpleExceptionReel GetException(string exceptionId);
        string RecordException(System.Exception simpleEception);
        void PlaybackException(string exceptionId);
    }
}