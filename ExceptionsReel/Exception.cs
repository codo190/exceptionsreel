﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Reflection;
using System.Transactions;

namespace ExceptionsReel
{
    /// <summary>
    /// this is a dumbed down exception, that can be serialized and deserialized
    /// by .NET Core's broken serialization.
    /// see (https://github.com/dotnet/corefx/issues/19119)
    /// 
    /// to use ExceptionsReel, any exceptions being saved must be converted to 
    /// this class.  then, in your exception handling code, try to cast
    /// your real life exception to SimpleException and handle that.
    /// </summary>
    /// <summary>
    /// this class contains the serialized SimpleException, along with 
    /// some meta data (time thrown, original class name, etc)
    /// </summary>
    public class SimpleExceptionReel
    {
        public long id;
        public DateTime? thrown_at;
        public string title;
        public string descr;
        public string exception_class;
        public string exception_object;
    }
}

namespace ExceptionsReel.Conversions
{
    /// <summary>
    /// these helpers basically take complicated (unserializable) exception objects and simplify
    /// them to the base Exception class, and take the unserializable data you'd want and stash
    /// it in the Exception's Data member (which is of type Dictionary<object,object>)
    /// 
    /// the list of data types that can be serialized are listed here:
    /// https://docs.microsoft.com/en-us/dotnet/standard/serialization/binary-serialization#binary-serialization-in-net-core
    /// 
    /// when simplifying complex exception types and their data, you need to restrict yourself to
    /// the classes listed in that file.  otherwise you will get inconsistent results upon
    /// deserialization
    /// </summary>
    public static class SimpleExceptionConversions
    {
        public static Exception Simplify(this System.Exception exception)
        {
            return exception;
        }
        public static Exception Simplify(this WebException webException)
        {
            var responseHeaders = webException.Response != null && webException.Response.Headers != null?
                webException.Response.Headers :
                null;
            /*
            var responseHeadersSimplified = responseHeaders
                .AllKeys
                .ToDictionary(k => k, k => responseHeaders[k]);
            */
            var responseHeadersSimplified = responseHeaders
                .AllKeys
                .Select(k => Tuple.Create(k,responseHeaders[k]))
                .ToList();
            var responseUri = webException.Response.ResponseUri;
            var responseContentType = webException.Response.ContentType;
            var responseStatus = webException.Status;
            
            var simpleException = webException as Exception;
            simpleException.Data.Add("responseHeaders",responseHeaders);
            simpleException.Data.Add("responseUri",responseUri);
            simpleException.Data.Add("responseContentType",responseContentType);
            simpleException.Data.Add("responseStatus",responseStatus);
            
            return simpleException;
        }
    }
}
