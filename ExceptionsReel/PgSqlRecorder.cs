﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using Npgsql;
using Dapper;   
using Dapper.Contrib;
using Dapper.Contrib.Extensions;
using ExceptionsReel.Conversions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ExceptionsReel
{
    public class PgSqlRecorder : IExceptionRecorder
    {
        public string connectionString;
        public string databaseName;
        
        public string RecordException(Exception simpleException)
        {
            var serializedSimpleException = JsonConvert.SerializeObject(simpleException);
            using(var connection = new NpgsqlConnection(connectionString))
            {
                var insertSql = String.Format(
                    "insert into {0} (thrown_at,title,descr,exception_class,exception_object) values (@thrown_at,@title,@descr,@exception_class,@exception_object) returning id;",
                    databaseName);
                var insertedId = connection.ExecuteScalar(
                    insertSql,
                    new
                    {
                        thrown_at = DateTime.Now,
                        title = "",
                        descr = "",
                        exception_class = simpleException.GetType().ToString(),
                        exception_object = serializedSimpleException,
                    });
                
                return Convert.ToInt64(insertedId).ToString();
            }
        }
        public SimpleExceptionReel GetException(string exceptionId)
        {
            using(var connection = new NpgsqlConnection(connectionString))
            {
                var selectSql = String.Format(
                    "select * from {0} where id = @id",
                    databaseName);
                var exception = connection
                    .Query<SimpleExceptionReel>(selectSql, new {id = Convert.ToInt64(exceptionId)})
                    .ToList()
                    .SingleOrDefault();
                return exception;
            }
        }
        public void PlaybackException(string exceptionId)
        {
            throw new System.NotImplementedException();
        }
    }
    
    public static class PgSqlRecorderHelpers
    {
        public static Exception ToException(this SimpleExceptionReel simpleException)
        {
            return JsonConvert.DeserializeObject<Exception>(simpleException.exception_object);
        }
    }
}