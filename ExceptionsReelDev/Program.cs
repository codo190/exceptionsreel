﻿using System;
using System.Net;
using System.Net.Http;
using ExceptionsReel;
using ExceptionsReel.Conversions;
using Exception = System.Exception;

namespace ExceptionsReelDev
{
    class Program
    {
        static void Main(string[] args)
        {
            var pgRecorder = new PgSqlRecorder()
            {
                connectionString = Environment.GetEnvironmentVariable("ErrorReel_pgsqlConnectionString"),
                databaseName = "simple_exception_reels",
            };
            var webException= new Func<WebException>(() =>
            {
                try
                {
                    var request = HttpWebRequest.CreateHttp("http://google.com/404");
                    var response = request.GetResponse();
                    return null;
                }
                catch (WebException e)
                {
                    return e;
                }
            })();
            
            var simpleException = webException.Simplify();
            var inserted_id = pgRecorder.RecordException(simpleException);
            
            var retrievedException = pgRecorder
                .GetException(inserted_id)
                .ToException();
        }
    }
}